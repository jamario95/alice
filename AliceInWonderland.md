# Alice's Adventures in Wonderland  
![Alice's Adventures in Wonderland](https://pbs.twimg.com/media/EAts-3NXYAQ-qrS.png)
## ALICE'S ADVENTURES IN WONDERLAND  

### BY LEWIS CARROLL
*WITH FORTY-TWO ILLUSTRATIONS BY JOHN TENNIEL*  
VolumeOne Publishing  
Chicago, Illinois 1998  

[A BookVirtual Digital Edition, v1.2](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf&ved=2ahUKEwjh8qGzo5uAAxX_dKQEHQH0CwYQFnoECBUQAQ&usg=AOvVaw1g3f6kLbJNHl5RuqBttq4a)  
November, 2000
![Alice's Adventures in Wonderland](https://www.gutenberg.org/files/19778/19778-h/images/frontipiece.jpg)
<pre>
All in the golden afternoon
  Full leisurely we glide;
For both our oars, with little skill,
  By little arms are plied,
While little hands make vain pretence
  Our wanderings to guide.



Ah, cruel Three! In such an hour,
  Beneath such dreamy weather,
To beg a tale of breath too weak
  To stir the tiniest feather!
Yet what can one poor voice avail
  Against three tongues together?



Imperious Prima flashes forth
  Her edict 'to begin it' –
In gentler tone Secunda hopes
  'There will be nonsense in it!' –
While Tertia interrupts the tale
  Not <em>more</em> than once a minute.



Anon, to sudden silence won,
  In fancy they pursue
The dream-child moving through a land
  Of wonders wild and new,
In friendly chat with bird or beast –
  And half believe it true.



And ever, as the story drained
  The wells of fancy dry,
And faintly strove that weary one
  To put the subject by,
"The rest next time -" "It <em>is</em> next time!"
  The happy voices cry.



Thus grew the tale of Wonderland:
  Thus slowly, one by one,
Its quaint events were hammered out –
  And now the tale is done,
And home we steer, a merry crew,
  Beneath the setting sun.



Alice! a childish story take,
  And with a gentle hand
Lay it were Childhood's dreams are twined
  In Memory's mystic band,
Like pilgrim's wither'd wreath of flowers
  Pluck'd in a far-off land.
</pre>

# Contents

[I. DOWN THE RABBIT-HOLE](#chapter-i)  
[II. THE POOL OF TEARS](#chapter-ii)  
[III. A CAUCUS-RACE AND A LONG TALE](#chapter-iii)  
[IV. THE RABBIT SENDS IN A LITTLE BILL](#chapter-iv)  
[V. ADVICE FROM A CATERPILLAR](#chapter-v)  
[VI. PIG AND PEPPER](#chapter-vi)  
[VII. A MAD TEA-PARTY](#chapter-vii)  
[VIII. THE QUEEN'S CROQUET-GROUND](#chapter-viii)  
[IX. THE MOCK TURTLE'S STORY](#chapter-ix)  
[X. THE LOBSTER QUADRILLE](#chapter-x)  
[XI. WHO STOLE THE TARTS?](#chapter-xi)  
[XII. ALICE'S EVIDENCE](#chapter-xii)  
--- 
1

![DOWN THE RABBIT-HOLE](https://www.gutenberg.org/files/19778/19778-h/images/p001.png)
## Chapter I
### DOWN THE RABBIT-HOLE

ALICE was beginning to get very tired of  
sitting by her sister on the bank, and of having  
nothing to do: once or twice she had peeped into  
the book her sister was reading, but it had no  
pictures or conversations in it, “ and what is  